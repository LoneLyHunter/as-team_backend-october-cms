<?php namespace ASTeam\Company\Models;

use Model;

/**
 * Model
 */
class GenderFamily extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
        'procent' => 'required',
        'icon' => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'asteam_company_gender_and_family';

    /* Relations */
    public $attachOne = [
        'icon' => 'System\Models\File'
    ];
}