<?php namespace ASTeam\Company\Models;

use Model;

/**
 * Model
 */
class Year extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
        'age' => 'required',
    ];

    protected $jsonable = ['blocks_procent'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'asteam_company_years';

    /* Relations */

    public $attachOne = [
        'icon' => 'System\Models\File'
    ];
}