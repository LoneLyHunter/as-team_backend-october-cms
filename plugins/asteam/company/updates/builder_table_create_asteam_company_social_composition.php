<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamCompanySocialComposition extends Migration
{
    public function up()
    {
        Schema::create('asteam_company_social_composition', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('procent', 55);
            $table->string('definition', 255);
            $table->integer('sort_order');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_company_social_composition');
    }
}
