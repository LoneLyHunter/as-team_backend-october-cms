<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamCompanyYears extends Migration
{
    public function up()
    {
        Schema::create('asteam_company_years', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('age', 155);
            $table->text('blocks_procent');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_company_years');
    }
}
