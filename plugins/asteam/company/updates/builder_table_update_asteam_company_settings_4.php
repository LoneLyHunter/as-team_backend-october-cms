<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamCompanySettings4 extends Migration
{
    public function up()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->string('first_slide', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->dropColumn('first_slide');
        });
    }
}
