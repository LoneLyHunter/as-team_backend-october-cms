<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamCompanyYears extends Migration
{
    public function up()
    {
        Schema::table('asteam_company_years', function($table)
        {
            $table->integer('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('asteam_company_years', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
