<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamCompanySettings extends Migration
{
    public function up()
    {
        Schema::create('asteam_company_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('email1', 255);
            $table->string('email2', 255);
            $table->string('telephone', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_company_settings');
    }
}
