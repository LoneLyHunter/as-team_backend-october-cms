<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamCompanySettings5 extends Migration
{
    public function up()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->string('address')->nullable();
            $table->string('email_contacts', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->dropColumn('address');
            $table->dropColumn('email_contacts');
        });
    }
}
