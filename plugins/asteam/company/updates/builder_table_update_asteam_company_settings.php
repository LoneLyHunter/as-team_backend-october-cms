<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamCompanySettings extends Migration
{
    public function up()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->string('email1', 255)->nullable()->change();
            $table->string('email2', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->string('email1', 255)->nullable(false)->change();
            $table->string('email2', 255)->nullable(false)->change();
        });
    }
}
