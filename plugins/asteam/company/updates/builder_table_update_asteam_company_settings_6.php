<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamCompanySettings6 extends Migration
{
    public function up()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }
}
