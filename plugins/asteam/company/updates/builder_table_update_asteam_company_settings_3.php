<?php namespace ASTeam\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamCompanySettings3 extends Migration
{
    public function up()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->string('image_about_1')->nullable();
            $table->string('image_about_2')->nullable();
            $table->string('image_screen')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_company_settings', function($table)
        {
            $table->dropColumn('image_about_1');
            $table->dropColumn('image_about_2');
            $table->dropColumn('image_screen');
        });
    }
}
