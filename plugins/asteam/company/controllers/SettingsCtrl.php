<?php namespace ASTeam\Company\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Flash;
use ASTeam\Company\Models\CompanySettings;

class SettingsCtrl extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('ASTeam.Company', 'menu-company', 'side-menu-item');

        // Подключаем скрипты медиабиблиотеки чтобы загружать фотографии в форме
        $this->addCss("/modules/cms/widgets/mediamanager/assets/css/mediamanager.css");
        $this->addCss("/modules/cms/formwidgets/mediafinder/assets/css/mediafinder.css");
        $this->addJs("/modules/cms/widgets/mediamanager/assets/js/mediamanager-browser-min.js");
        $this->addJs("/modules/backend/widgets/form/assets/js/october.form.js?");
        $this->addJs("/modules/cms/formwidgets/mediafinder/assets/js/mediafinder.js");
    }

    public $pageTitle = "Настройки компании";

    public function onUpdateSettings()
    {
        $data = post();
        if($data['image_1'] && $data['image_2'] && $data['image_screen'] && $data['first_slide'] && $data['lat'] && $data['lng'])
        {
            $settings = CompanySettings::first();
            if($settings)
            {
                // Удаляем лишние пути из файлов
                $image1_result = str_replace("/storage/app/media", "", $data['image_1']);
                $image2_result = str_replace("/storage/app/media", "", $data['image_2']);
                $image_screen = str_replace("/storage/app/media", "", $data['image_screen']);
                $first_slide = str_replace("/storage/app/media", "", $data['first_slide']);

                $settings->email1 = $data['email1'];
                $settings->email2 = $data['email2'];
                $settings->telephone = $data['telephone'];
                $settings->image_about_1 = $image1_result;
                $settings->image_about_2 = $image2_result;
                $settings->image_screen = $image_screen;
                $settings->first_slide = $first_slide;
                $settings->address = $data['address'];
                $settings->email_contacts = $data['email_contacts'];
                $settings->lat = $data['lat'];
                $settings->lng = $data['lng'];
                $settings->save();
                Flash::success("Данные успешно обновлены");
            } else {
                Flash::error("Запись не найдена в базе");
            }
        } else {
            Flash::error("Вы не заполнили обязательные поля, такие как: все изображения, позиции карты");
        }
    }
}