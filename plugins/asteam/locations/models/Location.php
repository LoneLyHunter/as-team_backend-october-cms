<?php namespace ASTeam\Locations\Models;

use Model;

/**
 * Model
 */
class Location extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    
    /*
     * Validation
     */
    public $rules = [
        'title' => 'required|max:255',
        'address' => 'required|max:255',
        'lat' => 'required|max:255',
        'lng' => 'required|max:255',
        'image_preview' => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'asteam_locations_';

    /* Relations */

    public $attachOne = [
        'image_preview' => 'System\Models\File'
    ];

    public $attachMany = [
        'images' => 'System\Models\File'
    ];
}