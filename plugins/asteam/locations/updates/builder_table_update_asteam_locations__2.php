<?php namespace ASTeam\Locations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamLocations2 extends Migration
{
    public function up()
    {
        Schema::table('asteam_locations_', function($table)
        {
            $table->renameColumn('adress', 'address');
        });
    }
    
    public function down()
    {
        Schema::table('asteam_locations_', function($table)
        {
            $table->renameColumn('address', 'adress');
        });
    }
}
