<?php namespace ASTeam\Locations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamLocations extends Migration
{
    public function up()
    {
        Schema::table('asteam_locations_', function($table)
        {
            $table->renameColumn('definition', 'additional');
        });
    }
    
    public function down()
    {
        Schema::table('asteam_locations_', function($table)
        {
            $table->renameColumn('additional', 'definition');
        });
    }
}
