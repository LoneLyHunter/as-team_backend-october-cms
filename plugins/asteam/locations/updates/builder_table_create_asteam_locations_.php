<?php namespace ASTeam\Locations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamLocations extends Migration
{
    public function up()
    {
        Schema::create('asteam_locations_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('lat', 255);
            $table->string('lng', 255);
            $table->string('title', 255);
            $table->text('adress');
            $table->text('definition');
            $table->integer('index_page')->default(0);
            $table->integer('sort_order');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_locations_');
    }
}
