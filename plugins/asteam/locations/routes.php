<?php
use ASTeam\Locations\Models\Location;
use System\Models\File;
use ASTeam\Company\Models\CompanySettings;

Route::get("api/locations", function() {
    $data = Location::orderBy("sort_order")->get();
    $resultCollection = $data->toArray();
    foreach ($data as $key => $itemCollection)
    {
        if(count($itemCollection->images) > 0)
        {
            $resultCollection[$key]['photos'] = true;
        } else {
            $resultCollection[$key]['photos'] = false;
        }
    }

    return response()->json($resultCollection, 200, array('Content-Type' => 'application/json; charset=UTF-8', 'charset' => 'utf-8'),JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
});

Route::get("api/photos/{id}", function($id) {
    $model = Location::find($id);
    if($model)
    {
        $result = [];
        foreach($model->images as $image)
        {
            $result[] = "<div class='col-12 col-sm-6 col-md-4'><div class='aste-popup-photo-wrapper' data-src-full='".$image->getPath()."'><img src='".$image->getThumb(350, 300, ['mode' => 'crop'])."' class='aste-popup-photo'></div></div>";
        }
        $resultStr = implode("", $result);

        return response("<div class='row aste-popup-row'>".$resultStr."</div>",200)->header('Content-Type','text/plain');
    }
});

Route::post("api/form", function() {
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $data = $_POST;
        if (trim(htmlspecialchars($data['name'])) && trim(htmlspecialchars($data['phone']))) {
            $resultVars['name'] = trim(htmlspecialchars($data['name']));
            $resultVars['telephone'] = trim(htmlspecialchars($data['phone']));
            if ($data['locations']) {
                $resultLocations = [];
                foreach ($data['locations'] as $locationId) {
                    $collection = Location::find($locationId);
                    if ($collection) {
                        $resultLocations[] = $collection->title;
                    }
                }

                $locationsStr = implode(",", $resultLocations);
                $resultVars['locations'] = $locationsStr;
            }
            $settings = CompanySettings::first();
            if($settings) {
                Mail::sendTo(["$settings->email1" => 'Email #1', "$settings->email2" => 'Email #2'], 'asteam.order-location', $resultVars);
            }

//            Mail::send('asteam.order-location', $resultVars, function ($message) {
//                $settings = CompanySettings::first();
//                if($settings)
//                {
//                    $message->to("$settings->email1");
//                    $message->cc("$settings->email2");
//                } else {
//                    return response("Не указано ни одной почты в настройках", 500);
//                }
//            });
            return response("true", 200);
        } else {
            return response("false", 500);
        }
    } else {
        return response("Запрос должен быть только ajax", 500);
    }
});