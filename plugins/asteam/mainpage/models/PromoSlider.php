<?php namespace ASTeam\Mainpage\Models;

use Model;

/**
 * Model
 */
class PromoSlider extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
        'promo_slide_image' => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'asteam_mainpage_promo_slider';

    public $attachOne = [
        'promo_pattern' => 'System\Models\File',
        'promo_slide_image' => 'System\Models\File'
    ];
}