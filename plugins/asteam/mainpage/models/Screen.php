<?php namespace ASTeam\Mainpage\Models;

use Model;

/**
 * Model
 */
class Screen extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $jsonable = ['blocks_info'];
    /*
     * Validation
     */
    public $rules = [
        'title' => 'required|max:255',
        'text' => 'required',
        'image_screen' => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'asteam_mainpage_screens';

    public $attachOne = [
        'image_screen' => 'System\Models\File'
    ];
}