<?php namespace ASTeam\Mainpage\Models;

use Model;
/**
 * Model
 */
class Slider extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
        'title_bold' => 'required|max:255',
        'title_definition' => 'max:255',
        'slide_image' => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'asteam_mainpage_slider';

    /* Relations */
    /*
     *  Для того чтобы в тип постов засунуть изображение не нужно его создавать в таблице, достаточно прописать
     *  name для поля в form fields и указать его здесь.
     * */
    public $attachOne = [
        'slide_image' => 'System\Models\File'
    ];
}