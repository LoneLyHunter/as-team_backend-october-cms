<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageScreens3 extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->integer('sort_order')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->integer('sort_order')->nullable(false)->change();
        });
    }
}
