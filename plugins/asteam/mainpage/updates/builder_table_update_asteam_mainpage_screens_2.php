<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageScreens2 extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->integer('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
