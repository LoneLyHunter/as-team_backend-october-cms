<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageScreens extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->string('url_link')->nullable();
            $table->string('text_link')->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->dropColumn('url_link');
            $table->dropColumn('text_link');
            $table->increments('id')->unsigned()->change();
        });
    }
}
