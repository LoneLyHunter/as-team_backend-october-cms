<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamMainpageAudience extends Migration
{
    public function up()
    {
        Schema::create('asteam_mainpage_audience', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('procent', 55);
            $table->string('definition', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_mainpage_audience');
    }
}
