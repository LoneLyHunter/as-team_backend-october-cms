<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageSlider4 extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->integer('position')->nullable(false)->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->integer('position')->nullable()->unsigned(false)->change();
        });
    }
}
