<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageSlider9 extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->integer('position')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->integer('position')->default(1)->change();
        });
    }
}
