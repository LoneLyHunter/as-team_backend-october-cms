<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageScreens4 extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->text('blocks_info');
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_screens', function($table)
        {
            $table->dropColumn('blocks_info');
        });
    }
}
