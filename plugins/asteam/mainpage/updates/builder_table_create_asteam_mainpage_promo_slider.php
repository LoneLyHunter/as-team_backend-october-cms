<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamMainpagePromoSlider extends Migration
{
    public function up()
    {
        Schema::create('asteam_mainpage_promo_slider', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_mainpage_promo_slider');
    }
}
