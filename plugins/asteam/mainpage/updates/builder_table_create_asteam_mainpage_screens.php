<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamMainpageScreens extends Migration
{
    public function up()
    {
        Schema::create('asteam_mainpage_screens', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255);
            $table->text('text');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_mainpage_screens');
    }
}
