<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageSlider2 extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->string('title_definition', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->string('title_definition', 255)->nullable(false)->change();
        });
    }
}
