<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpagePromoSlider extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_promo_slider', function($table)
        {
            $table->dropColumn('title');
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_promo_slider', function($table)
        {
            $table->string('title', 255)->nullable();
        });
    }
}
