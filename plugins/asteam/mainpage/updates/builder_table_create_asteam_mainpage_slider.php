<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAsteamMainpageSlider extends Migration
{
    public function up()
    {
        Schema::create('asteam_mainpage_slider', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('image');
            $table->string('title_bold');
            $table->string('title_definition');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('asteam_mainpage_slider');
    }
}
