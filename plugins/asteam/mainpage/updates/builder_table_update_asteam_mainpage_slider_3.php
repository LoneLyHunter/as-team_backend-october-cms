<?php namespace ASTeam\Mainpage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAsteamMainpageSlider3 extends Migration
{
    public function up()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->integer('position')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('asteam_mainpage_slider', function($table)
        {
            $table->dropColumn('position');
        });
    }
}
