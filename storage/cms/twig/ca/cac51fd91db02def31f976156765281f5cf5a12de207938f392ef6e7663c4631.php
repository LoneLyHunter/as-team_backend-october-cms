<?php

/* /var/www/www-root/data/www/october.asteam/themes/as-team/pages/contacts.htm */
class __TwigTemplate_f146e08feb0101bd8c9334740c21d313f03bce278b4d9c75f9dd69ddc54facc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"astb-contacts\">
    <div class=\"container\">
        <div class=\"aste-primary\">Связаться с нами</div>
        <div class=\"aste-secondary\">Наши контакты</div>
        <div class=\"aste-map-info\">
            <div class=\"aste-map\" data-lat=\"";
        // line 6
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["setting"] ?? null), "lat", array())) ? ($this->getAttribute(($context["setting"] ?? null), "lat", array())) : ("55.811600")), "html", null, true);
        echo "\" data-lng=\"";
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["setting"] ?? null), "lng", array())) ? ($this->getAttribute(($context["setting"] ?? null), "lng", array())) : ("37.456668")), "html", null, true);
        echo "\"></div>
            <div class=\"aste-info\">
                <div class=\"aste-info-title\">Офис в Москве</div>
                <div class=\"aste-info-value\">";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["setting"] ?? null), "address", array()), "html", null, true);
        echo "</div>
                <div class=\"aste-info-title\">Контактный телефон</div>
                <div class=\"aste-info-value\">";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute(($context["setting"] ?? null), "telephone", array()), "html", null, true);
        echo "</div>
                <div class=\"aste-info-title\">Адрес электронной почты</div>
                <div class=\"aste-info-value\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["setting"] ?? null), "email_contacts", array()), "html", null, true);
        echo "</div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/contacts.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 13,  39 => 11,  34 => 9,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"astb-contacts\">
    <div class=\"container\">
        <div class=\"aste-primary\">Связаться с нами</div>
        <div class=\"aste-secondary\">Наши контакты</div>
        <div class=\"aste-map-info\">
            <div class=\"aste-map\" data-lat=\"{{ (setting.lat) ? setting.lat : '55.811600' }}\" data-lng=\"{{ (setting.lng) ? setting.lng : '37.456668' }}\"></div>
            <div class=\"aste-info\">
                <div class=\"aste-info-title\">Офис в Москве</div>
                <div class=\"aste-info-value\">{{ setting.address }}</div>
                <div class=\"aste-info-title\">Контактный телефон</div>
                <div class=\"aste-info-value\">{{ setting.telephone }}</div>
                <div class=\"aste-info-title\">Адрес электронной почты</div>
                <div class=\"aste-info-value\">{{ setting.email_contacts }}</div>
            </div>
        </div>
    </div>
</div>", "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/contacts.htm", "");
    }
}
