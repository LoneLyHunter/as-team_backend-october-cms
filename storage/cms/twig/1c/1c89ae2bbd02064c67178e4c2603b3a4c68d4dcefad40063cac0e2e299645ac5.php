<?php

/* /var/www/www-root/data/www/october.asteam/themes/as-team/partials/header.htm */
class __TwigTemplate_a6578bd0e96c5c8b875860349a3962466c7034f051d9fea8062e961ee6a77e0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <title>AS Team - ";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">
    <meta name=\"title\" content=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "meta_title", array()), "html", null, true);
        echo "\">
    <link href=\"";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/styles.css");
        echo "\" rel=\"stylesheet\">
    ";
        // line 10
        echo $this->env->getExtension('CMS')->assetsFunction('css');
        echo $this->env->getExtension('CMS')->displayBlock('styles');
        // line 11
        echo "</head>
<body>



<nav class=\"navbar navbar-expand-md astb-navbar ";
        // line 16
        if (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()) == "home")) {
            echo "astm-main-page";
        }
        echo "\">
    <div class=\"container\">
        <a href=\"/\" class=\"navbar-brand aste-brand\"><img src=\"";
        // line 18
        if (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()) == "home")) {
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/logo.svg");
        } else {
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/logo-colour.svg");
        }
        echo "\" alt=\"Залог Инвест\"></a>
        <button class=\"navbar-toggler aste-navbar-toggler-button\" type=\"button\" data-toggle=\"collapse\" data-target=\"#astNavbar\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon aste-navbar-toggler\">
            <div class=\"aste-icon-bar\"></div>
            <div class=\"aste-icon-bar\"></div>
            <div class=\"aste-icon-bar\"></div>
        </span>
        </button>
        <div class=\"collapse navbar-collapse aste-navbar-inner\" id=\"astNavbar\">
            <ul class=\"navbar-nav aste-navbar-list\">
                <li class=\"nav-item aste-navitem ";
        // line 28
        if (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()) == "company")) {
            echo "astm-active";
        }
        echo "\">
                    <a class=\"nav-link aste-navlink\" href=\"";
        // line 29
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("company");
        echo "\">Компания</a>
                </li>
                <li class=\"nav-item aste-navitem ";
        // line 31
        if (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()) == "locations")) {
            echo "astm-active";
        }
        echo "\">
                    <a class=\"nav-link aste-navlink\" href=\"";
        // line 32
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("locations");
        echo "\">Места размещения</a>
                </li>
                <li class=\"nav-item aste-navitem ";
        // line 34
        if (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()) == "contacts")) {
            echo "astm-active";
        }
        echo "\">
                    <a class=\"nav-link aste-navlink\" href=\"";
        // line 35
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("contacts");
        echo "\">Связаться с нами</a>
                </li>
            </ul>
        </div>
    </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "/var/www/www-root/data/www/october.asteam/themes/as-team/partials/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 35,  98 => 34,  93 => 32,  87 => 31,  82 => 29,  76 => 28,  59 => 18,  52 => 16,  45 => 11,  42 => 10,  38 => 9,  34 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <title>AS Team - {{ this.page.title }}</title>
    <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
    <meta name=\"title\" content=\"{{ this.page.meta_title }}\">
    <link href=\"{{ 'assets/css/styles.css'|theme }}\" rel=\"stylesheet\">
    {% styles %}
</head>
<body>



<nav class=\"navbar navbar-expand-md astb-navbar {% if this.page.id == 'home' %}astm-main-page{% endif %}\">
    <div class=\"container\">
        <a href=\"/\" class=\"navbar-brand aste-brand\"><img src=\"{% if this.page.id == 'home' %}{{ 'assets/img/logo.svg'|theme }}{% else %}{{ 'assets/img/logo-colour.svg'|theme }}{% endif %}\" alt=\"Залог Инвест\"></a>
        <button class=\"navbar-toggler aste-navbar-toggler-button\" type=\"button\" data-toggle=\"collapse\" data-target=\"#astNavbar\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon aste-navbar-toggler\">
            <div class=\"aste-icon-bar\"></div>
            <div class=\"aste-icon-bar\"></div>
            <div class=\"aste-icon-bar\"></div>
        </span>
        </button>
        <div class=\"collapse navbar-collapse aste-navbar-inner\" id=\"astNavbar\">
            <ul class=\"navbar-nav aste-navbar-list\">
                <li class=\"nav-item aste-navitem {% if this.page.id == 'company' %}astm-active{% endif %}\">
                    <a class=\"nav-link aste-navlink\" href=\"{{ 'company'|page }}\">Компания</a>
                </li>
                <li class=\"nav-item aste-navitem {% if this.page.id == 'locations' %}astm-active{% endif %}\">
                    <a class=\"nav-link aste-navlink\" href=\"{{ 'locations'|page }}\">Места размещения</a>
                </li>
                <li class=\"nav-item aste-navitem {% if this.page.id == 'contacts' %}astm-active{% endif %}\">
                    <a class=\"nav-link aste-navlink\" href=\"{{ 'contacts'|page }}\">Связаться с нами</a>
                </li>
            </ul>
        </div>
    </div>
</nav>", "/var/www/www-root/data/www/october.asteam/themes/as-team/partials/header.htm", "");
    }
}
