<?php

/* /var/www/www-root/data/www/october.asteam/themes/as-team/pages/locations.htm */
class __TwigTemplate_34ea5fe62174e4b42e8f41315f453d83ba97bfbb4371b78ed723258df10e32b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"astb-locations\">
    <div class=\"aste-listmap-switcher\">
        <div class=\"aste-switcher-switch astm-switched astm-list\">Списком</div>
        <div class=\"aste-switcher-switch astm-map\">На карте</div>
    </div>
    <div class=\"aste-locations-list-wrapper\">
        <h2 class=\"aste-locations-list-title\">Жилые комплексы</h2>
        <ul class=\"aste-locations-list\">
            <!-- Here be locations -->
        </ul>
    </div>
    <div class=\"aste-locations-map-wrapper\">
        <div class=\"aste-locations-map\"></div>
    </div>
    <div class=\"aste-locations-submit\">Заказать размещение</div>
</div>




<div class=\"astb-popup\">
    <div class=\"aste-popup-inner\">
    </div>
    <div class=\"aste-subpopup\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/locations.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"astb-locations\">
    <div class=\"aste-listmap-switcher\">
        <div class=\"aste-switcher-switch astm-switched astm-list\">Списком</div>
        <div class=\"aste-switcher-switch astm-map\">На карте</div>
    </div>
    <div class=\"aste-locations-list-wrapper\">
        <h2 class=\"aste-locations-list-title\">Жилые комплексы</h2>
        <ul class=\"aste-locations-list\">
            <!-- Here be locations -->
        </ul>
    </div>
    <div class=\"aste-locations-map-wrapper\">
        <div class=\"aste-locations-map\"></div>
    </div>
    <div class=\"aste-locations-submit\">Заказать размещение</div>
</div>




<div class=\"astb-popup\">
    <div class=\"aste-popup-inner\">
    </div>
    <div class=\"aste-subpopup\"></div>
</div>", "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/locations.htm", "");
    }
}
