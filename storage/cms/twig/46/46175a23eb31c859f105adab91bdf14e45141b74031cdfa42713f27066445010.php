<?php

/* /var/www/www-root/data/www/october.asteam/themes/as-team/pages/company.htm */
class __TwigTemplate_e6f016d385965fcb2ae3c419daa0fc3e20b992c1420169e67b279b0cc9a784dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["socials"] = $this->getAttribute(($context["Social"] ?? null), "records", array());
        // line 2
        $context["years"] = $this->getAttribute(($context["Years"] ?? null), "records", array());
        // line 3
        $context["families"] = $this->getAttribute(($context["GenderFamily"] ?? null), "records", array());
        // line 4
        echo "
<div class=\"astb-company-first-screen\">
    <h1 class=\"aste-primary\">";
        // line 6
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_bold_marketing"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h1>
    <h2 class=\"aste-secondary\">";
        // line 7
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_definition_marketing"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
    ";
        // line 8
        if ($this->getAttribute(($context["settings"] ?? null), "first_slide", array())) {
            // line 9
            echo "        <img src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->mediaFilter($this->getAttribute(($context["settings"] ?? null), "first_slide", array()));
            echo "\" class=\"aste-first-screen-image\">
    ";
        }
        // line 11
        echo "</div>



<div class=\"astb-company-paging\">
    <div class=\"aste-paging-navbar\">
        <ul class=\"aste-navbar-list\">
            <li class=\"aste-navitem\"><a href=\"#page-1\" class=\"aste-navlink\">О компании</a></li>
            <li class=\"aste-navitem\"><a href=\"#page-2\" class=\"aste-navlink\">Наша аудитория</a></li>
            <li class=\"aste-navitem\"><a href=\"#page-3\" class=\"aste-navlink\">Наши экраны</a></li>
        </ul>
    </div>
    <div class=\"aste-company-page astm-page-1\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">";
        // line 27
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_bold_about"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
                    <h2 class=\"aste-secondary\">";
        // line 28
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_definition_about"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
                    <p class=\"aste-page-text\">";
        // line 29
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("text_about"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</p>
                </div>
                ";
        // line 31
        if ($this->getAttribute(($context["settings"] ?? null), "image_about_1", array())) {
            // line 32
            echo "                    <div class=\"col-12 col-md-6\"><img src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->mediaFilter($this->getAttribute(($context["settings"] ?? null), "image_about_1", array()));
            echo "\" alt=\"\" class=\"aste-page-image\"></div>
                ";
        }
        // line 34
        echo "                ";
        if ($this->getAttribute(($context["settings"] ?? null), "image_about_2", array())) {
            // line 35
            echo "                    <div class=\"col-12 col-md-6\"><img src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->mediaFilter($this->getAttribute(($context["settings"] ?? null), "image_about_2", array()));
            echo "\" alt=\"\" class=\"aste-page-image\"></div>
                ";
        }
        // line 37
        echo "            </div>
        </div>
    </div>
    <div class=\"aste-company-page astm-page-2\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">";
        // line 44
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_bold_audience"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
                    <h2 class=\"aste-secondary\">";
        // line 45
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_definition_audience"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
                    <p class=\"aste-page-text\">";
        // line 46
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("text_audience"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</p>
                    <div class=\"astb-social-graphs\">
                        ";
        // line 48
        if ((twig_length_filter($this->env, ($context["socials"] ?? null)) > 0)) {
            // line 49
            echo "                            <div class=\"aste-social-graph\">
                                <div class=\"aste-social-graph-title\">Социальный состав</div>
                                <div class=\"row aste-social-graph-row\">
                                    ";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["socials"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["itemSocial"]) {
                // line 53
                echo "                                        <div class=\"col-6 col-md-3\">
                                            <div class=\"aste-graph-item\">
                                                <div class=\"aste-item-number\">";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemSocial"], "procent", array()), "html", null, true);
                echo "</div>
                                                <div class=\"aste-item-title\">";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemSocial"], "definition", array()), "html", null, true);
                echo "</div>
                                            </div>
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemSocial'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                                </div>
                            </div>
                        ";
        }
        // line 63
        echo "                        ";
        if ((twig_length_filter($this->env, ($context["families"] ?? null)) > 0)) {
            // line 64
            echo "                            <div class=\"aste-social-graph\">
                                <div class=\"aste-social-graph-title\">Пол и семья</div>
                                <div class=\"row aste-social-graph-row\">
                                    ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["families"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["itemFamily"]) {
                // line 68
                echo "                                        <div class=\"col-6 col-md-3\">
                                            <div class=\"aste-graph-item\">
                                                <img src=\"";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemFamily"], "icon", array()), "path", array()), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemFamily"], "icon", array()), "description", array()), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemFamily"], "icon", array()), "title", array()), "html", null, true);
                echo "\" class=\"aste-item-image\">
                                                <div class=\"aste-item-number\">";
                // line 71
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemFamily"], "procent", array()), "html", null, true);
                echo "</div>
                                                <div class=\"aste-item-title\">";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemFamily"], "definition", array()), "html", null, true);
                echo "</div>
                                            </div>
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemFamily'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo "                                </div>
                            </div>
                        ";
        }
        // line 79
        echo "                        ";
        if ((twig_length_filter($this->env, ($context["years"] ?? null)) > 0)) {
            // line 80
            echo "                            <div class=\"aste-social-graph\">
                                <div class=\"aste-social-graph-title\">Возраст</div>
                                <div class=\"row aste-social-graph-row\">
                                    ";
            // line 83
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["years"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["itemYear"]) {
                // line 84
                echo "                                        <div class=\"col-12 col-md-4\">
                                            <div class=\"aste-graph-item\">
                                                <div class=\"aste-item-label\">";
                // line 86
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemYear"], "age", array()), "html", null, true);
                echo "</div>
                                                ";
                // line 87
                if ($this->getAttribute($context["itemYear"], "blocks_procent", array())) {
                    // line 88
                    echo "                                                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["itemYear"], "blocks_procent", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["itemBlock"]) {
                        // line 89
                        echo "                                                        <div class=\"aste-item-half\">
                                                            <img src=\"";
                        // line 90
                        echo $this->env->getExtension('Cms\Twig\Extension')->mediaFilter($this->getAttribute($context["itemBlock"], "icon", array()));
                        echo "\" alt=\"";
                        echo $this->getAttribute($this->getAttribute($context["itemBlock"], "icon", array()), "description", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($this->getAttribute($context["itemBlock"], "icon", array()), "title", array());
                        echo "\" class=\"aste-item-image\">
                                                            <div class=\"aste-item-number\">";
                        // line 91
                        echo $this->getAttribute($context["itemBlock"], "procent", array());
                        echo "</div>
                                                            <div class=\"aste-item-title\">";
                        // line 92
                        echo $this->getAttribute($context["itemBlock"], "definition", array());
                        echo "</div>
                                                        </div>
                                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemBlock'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 95
                    echo "                                                ";
                }
                // line 96
                echo "                                            </div>
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemYear'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 99
            echo "                                </div>
                            </div>
                        ";
        }
        // line 102
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"aste-company-page astm-page-3\" id=\"page-3\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">";
        // line 111
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_bold_screens"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
                    <h2 class=\"aste-secondary\">";
        // line 112
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_definition_screens"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
                    <p class=\"aste-page-text\">";
        // line 113
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("text_screens"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</p>
                    ";
        // line 114
        if ($this->getAttribute(($context["settings"] ?? null), "image_screen", array())) {
            // line 115
            echo "                        <img src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->mediaFilter($this->getAttribute(($context["settings"] ?? null), "image_screen", array()));
            echo "\" alt=\"\" class=\"aste-page-image\">
                    ";
        }
        // line 117
        echo "                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/company.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 117,  306 => 115,  304 => 114,  298 => 113,  292 => 112,  286 => 111,  275 => 102,  270 => 99,  262 => 96,  259 => 95,  250 => 92,  246 => 91,  238 => 90,  235 => 89,  230 => 88,  228 => 87,  224 => 86,  220 => 84,  216 => 83,  211 => 80,  208 => 79,  203 => 76,  193 => 72,  189 => 71,  181 => 70,  177 => 68,  173 => 67,  168 => 64,  165 => 63,  160 => 60,  150 => 56,  146 => 55,  142 => 53,  138 => 52,  133 => 49,  131 => 48,  124 => 46,  118 => 45,  112 => 44,  103 => 37,  97 => 35,  94 => 34,  88 => 32,  86 => 31,  79 => 29,  73 => 28,  67 => 27,  49 => 11,  43 => 9,  41 => 8,  35 => 7,  29 => 6,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set socials = Social.records %}
{% set years = Years.records %}
{% set families = GenderFamily.records %}

<div class=\"astb-company-first-screen\">
    <h1 class=\"aste-primary\">{% component 'title_bold_marketing' %}</h1>
    <h2 class=\"aste-secondary\">{% component 'title_definition_marketing' %}</h2>
    {% if settings.first_slide %}
        <img src=\"{{ settings.first_slide|media }}\" class=\"aste-first-screen-image\">
    {% endif %}
</div>



<div class=\"astb-company-paging\">
    <div class=\"aste-paging-navbar\">
        <ul class=\"aste-navbar-list\">
            <li class=\"aste-navitem\"><a href=\"#page-1\" class=\"aste-navlink\">О компании</a></li>
            <li class=\"aste-navitem\"><a href=\"#page-2\" class=\"aste-navlink\">Наша аудитория</a></li>
            <li class=\"aste-navitem\"><a href=\"#page-3\" class=\"aste-navlink\">Наши экраны</a></li>
        </ul>
    </div>
    <div class=\"aste-company-page astm-page-1\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">{% component 'title_bold_about' %}</h2>
                    <h2 class=\"aste-secondary\">{% component 'title_definition_about' %}</h2>
                    <p class=\"aste-page-text\">{% component 'text_about' %}</p>
                </div>
                {% if settings.image_about_1 %}
                    <div class=\"col-12 col-md-6\"><img src=\"{{ settings.image_about_1|media }}\" alt=\"\" class=\"aste-page-image\"></div>
                {% endif %}
                {% if settings.image_about_2 %}
                    <div class=\"col-12 col-md-6\"><img src=\"{{ settings.image_about_2|media }}\" alt=\"\" class=\"aste-page-image\"></div>
                {% endif %}
            </div>
        </div>
    </div>
    <div class=\"aste-company-page astm-page-2\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">{% component 'title_bold_audience' %}</h2>
                    <h2 class=\"aste-secondary\">{% component 'title_definition_audience' %}</h2>
                    <p class=\"aste-page-text\">{% component 'text_audience' %}</p>
                    <div class=\"astb-social-graphs\">
                        {% if socials|length > 0 %}
                            <div class=\"aste-social-graph\">
                                <div class=\"aste-social-graph-title\">Социальный состав</div>
                                <div class=\"row aste-social-graph-row\">
                                    {% for itemSocial in socials %}
                                        <div class=\"col-6 col-md-3\">
                                            <div class=\"aste-graph-item\">
                                                <div class=\"aste-item-number\">{{ itemSocial.procent }}</div>
                                                <div class=\"aste-item-title\">{{ itemSocial.definition }}</div>
                                            </div>
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>
                        {% endif %}
                        {% if families|length > 0 %}
                            <div class=\"aste-social-graph\">
                                <div class=\"aste-social-graph-title\">Пол и семья</div>
                                <div class=\"row aste-social-graph-row\">
                                    {% for itemFamily in families %}
                                        <div class=\"col-6 col-md-3\">
                                            <div class=\"aste-graph-item\">
                                                <img src=\"{{ itemFamily.icon.path }}\" alt=\"{{ itemFamily.icon.description }}\" title=\"{{ itemFamily.icon.title }}\" class=\"aste-item-image\">
                                                <div class=\"aste-item-number\">{{ itemFamily.procent }}</div>
                                                <div class=\"aste-item-title\">{{ itemFamily.definition }}</div>
                                            </div>
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>
                        {% endif %}
                        {% if years|length > 0 %}
                            <div class=\"aste-social-graph\">
                                <div class=\"aste-social-graph-title\">Возраст</div>
                                <div class=\"row aste-social-graph-row\">
                                    {% for itemYear in years %}
                                        <div class=\"col-12 col-md-4\">
                                            <div class=\"aste-graph-item\">
                                                <div class=\"aste-item-label\">{{ itemYear.age }}</div>
                                                {% if itemYear.blocks_procent %}
                                                    {% for itemBlock in itemYear.blocks_procent %}
                                                        <div class=\"aste-item-half\">
                                                            <img src=\"{{ itemBlock.icon|media }}\" alt=\"{{ itemBlock.icon.description | raw }}\" title=\"{{ itemBlock.icon.title | raw }}\" class=\"aste-item-image\">
                                                            <div class=\"aste-item-number\">{{ itemBlock.procent | raw }}</div>
                                                            <div class=\"aste-item-title\">{{ itemBlock.definition | raw }}</div>
                                                        </div>
                                                    {% endfor %}
                                                {% endif %}
                                            </div>
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"aste-company-page astm-page-3\" id=\"page-3\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">{% component 'title_bold_screens' %}</h2>
                    <h2 class=\"aste-secondary\">{% component 'title_definition_screens' %}</h2>
                    <p class=\"aste-page-text\">{% component 'text_screens' %}</p>
                    {% if settings.image_screen %}
                        <img src=\"{{ settings.image_screen|media }}\" alt=\"\" class=\"aste-page-image\">
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>", "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/company.htm", "");
    }
}
