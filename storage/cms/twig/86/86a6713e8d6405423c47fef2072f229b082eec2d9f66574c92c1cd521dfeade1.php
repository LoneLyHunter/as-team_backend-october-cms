<?php

/* /var/www/www-root/data/www/october.asteam/themes/as-team/partials/footer.htm */
class __TwigTemplate_d2f55e3ad2cb0f27fb1c54ee1976e78551b1f343d439758b90f5173d8aac79fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"astb-footer\">
    <div class=\"container aste-footer-container\">
        <div class=\"aste-copyright-self\">Copyright © 2017 AS-Team. Все права защищены.</div>
        <a href=\"https://plan-b.agency/\" target=\"_blank\" class=\"aste-copyright-dev\">Разработано в Plan B</a>
    </div>
</div>
<!-- Scripts -->
<script src=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/jquery.js");
        echo "\"></script>
<script src=\"";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/bootstrap.js");
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 10
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/scripts.js");
        echo "\"></script>
";
        // line 11
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 12
        echo $this->env->getExtension('CMS')->assetsFunction('js');
        echo $this->env->getExtension('CMS')->displayBlock('scripts');
        // line 13
        echo "</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/var/www/www-root/data/www/october.asteam/themes/as-team/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 13,  47 => 12,  40 => 11,  36 => 10,  32 => 9,  28 => 8,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"astb-footer\">
    <div class=\"container aste-footer-container\">
        <div class=\"aste-copyright-self\">Copyright © 2017 AS-Team. Все права защищены.</div>
        <a href=\"https://plan-b.agency/\" target=\"_blank\" class=\"aste-copyright-dev\">Разработано в Plan B</a>
    </div>
</div>
<!-- Scripts -->
<script src=\"{{ 'assets/vendor/jquery.js'|theme }}\"></script>
<script src=\"{{ 'assets/vendor/bootstrap.js'|theme }}\"></script>
<script type=\"text/javascript\" src=\"{{ 'assets/js/scripts.js'|theme }}\"></script>
{% framework extras %}
{% scripts %}
</body>
</html>", "/var/www/www-root/data/www/october.asteam/themes/as-team/partials/footer.htm", "");
    }
}
