<?php

/* /var/www/www-root/data/www/october.asteam/plugins/rainlab/editable/components/editable/default.htm */
class __TwigTemplate_62ba4f6f6662d396c961264bd5a0a8a6d9558db093904f77e0fa84b8e6a37cad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div
    data-control=\"editable\"
    data-handler=\"";
        // line 3
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "::onSave\"
    data-file=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute(($context["__SELF__"] ?? null), "file", array()), "html", null, true);
        echo "\"
    data-file-mode=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute(($context["__SELF__"] ?? null), "fileMode", array()), "html", null, true);
        echo "\">
    ";
        // line 6
        echo $this->getAttribute(($context["__SELF__"] ?? null), "content", array());
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/www-root/data/www/october.asteam/plugins/rainlab/editable/components/editable/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  31 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div
    data-control=\"editable\"
    data-handler=\"{{ __SELF__ }}::onSave\"
    data-file=\"{{ __SELF__.file }}\"
    data-file-mode=\"{{ __SELF__.fileMode }}\">
    {{ __SELF__.content|raw }}
</div>
", "/var/www/www-root/data/www/october.asteam/plugins/rainlab/editable/components/editable/default.htm", "");
    }
}
