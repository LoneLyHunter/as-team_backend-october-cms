<?php

/* /var/www/www-root/data/www/october.asteam/themes/as-team/pages/home.htm */
class __TwigTemplate_79c1a0e3f155e2b74a1ff6a27d34d2d02801d4079661491d02531c9b96da7141 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["recordsProcents"] = $this->getAttribute(($context["ProcentBlocks"] ?? null), "records", array());
        // line 2
        echo "
";
        // line 3
        $context["recordsPromo"] = $this->getAttribute(($context["PromoSlider"] ?? null), "records", array());
        // line 4
        echo "
";
        // line 5
        $context["recordsScreens"] = $this->getAttribute(($context["Screens"] ?? null), "records", array());
        // line 6
        echo "
";
        // line 7
        $context["recordsSlider"] = $this->getAttribute(($context["Slider"] ?? null), "records", array());
        // line 8
        echo "

<div class=\"astb-header-slider\">
    <div class=\"aste-arrows\">
        <div class=\"aste-arrow astm-left\"></div>
        <div class=\"aste-container-sized-delimiter container\"></div>
        <div class=\"aste-arrow astm-right\"></div>
    </div>
    ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["recordsSlider"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["itemSlider"]) {
            // line 17
            echo "        <div class=\"aste-slider-block\">
            <img class=\"aste-block-bg\" src=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemSlider"], "slide_image", array()), "path", array()), "html", null, true);
            echo "\" />
            <div class=\"aste-block-shade\"></div>
            <div class=\"aste-block-text\">
                <h2 class=\"aste-primary\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["itemSlider"], "title_bold", array()), "html", null, true);
            echo "</h2>
                <h3 class=\"aste-secondary\">";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["itemSlider"], "title_definition", array()), "html", null, true);
            echo "</h3>
            </div>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemSlider'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "</div>



<div class=\"astb-main-description\">
    <div class=\"container aste-description-container\">
        <div class=\"aste-description-left astm-border-right\">
            <h1 class=\"aste-primary\">";
        // line 33
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_bold_company"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h1>
            <h2 class=\"aste-secondary\">";
        // line 34
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_definition_company"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
            <p class=\"aste-description\">";
        // line 35
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("text_company"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</p>
        </div>
        <div class=\"aste-description-right\">
            <div class=\"aste-percentages\">
                ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["recordsProcents"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["itemProcent"]) {
            // line 40
            echo "                    <div class=\"aste-percentage\">
                        <div class=\"aste-number\">";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["itemProcent"], "procent", array()), "html", null, true);
            echo "</div>
                        <div class=\"aste-percentage-title\">";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["itemProcent"], "definition", array()), "html", null, true);
            echo "</div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemProcent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "            </div>
        </div>
        <div class=\"aste-description-left astm-description-link\">";
        // line 47
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("link_about"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</div>
        <div class=\"aste-description-right astm-description-link\">";
        // line 48
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("link_audience"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</div>
    </div>
</div>



<div class=\"astb-screens-slider\">
    <div class=\"aste-slider-track\">
        ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["recordsPromo"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["itemPromo"]) {
            // line 57
            echo "            <div class=\"aste-screen-wrapper\">
                <div class=\"aste-frame\"><img src=\"";
            // line 58
            echo call_user_func_array($this->env->getFilter('resize')->getCallable(), array($this->getAttribute($this->getAttribute($context["itemPromo"], "promo_pattern", array()), "path", array()), 290, 20));
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemPromo"], "promo_pattern", array()), "description", array()), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemPromo"], "promo_pattern", array()), "title", array()), "html", null, true);
            echo "\"class=\"aste-frame-texture\"></div>
                <div class=\"aste-screen\"><img src=\"";
            // line 59
            echo call_user_func_array($this->env->getFilter('resize')->getCallable(), array($this->getAttribute($this->getAttribute($context["itemPromo"], "promo_slide_image", array()), "path", array()), 290, 520));
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemPromo"], "promo_slide_image", array()), "description", array()), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemPromo"], "promo_slide_image", array()), "title", array()), "html", null, true);
            echo "\" class=\"aste-screen-image\"></div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemPromo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "    </div>
</div>


<div class=\"astb-screens-info\">
    <div class=\"aste-info-introductory\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">";
        // line 71
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_bold_screens"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
                    <h3 class=\"aste-secondary\">";
        // line 72
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_definition_screens"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h3>
                    <p class=\"aste-introductory-text\">";
        // line 73
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("text_screens"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"aste-info-blocks\">
        ";
        // line 79
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["recordsScreens"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["itemScreen"]) {
            // line 80
            echo "            <div class=\"aste-info-block\">
                <div class=\"container\">
                    <div class=\"row aste-block-row\">
                        <div class=\"d-none d-lg-block col-2\"></div>
                        <div class=\"col-4 col-lg-3\">
                            <div class=\"aste-block-image-wrapper\"><img src=\"";
            // line 85
            echo call_user_func_array($this->env->getFilter('resize')->getCallable(), array($this->getAttribute($this->getAttribute($context["itemScreen"], "image_screen", array()), "path", array()), 270, 478));
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["itemScreen"], "image_screen", array()), "description", array()), "html", null, true);
            echo "\" class=\"aste-block-image\"></div>
                        </div>
                        <div class=\"d-none d-sm-block col-sm-1\"></div>
                        <div class=\"col-8 col-sm-7 col-lg-4\">
                            <div class=\"aste-block-content\">
                                ";
            // line 90
            if ($this->getAttribute($context["itemScreen"], "title", array())) {
                // line 91
                echo "                                    <h4 class=\"aste-block-title\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemScreen"], "title", array()), "html", null, true);
                echo "</h4>
                                ";
            }
            // line 93
            echo "                                ";
            if ($this->getAttribute($context["itemScreen"], "text", array())) {
                // line 94
                echo "                                    <p class=\"aste-block-text\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemScreen"], "text", array()), "html", null, true);
                echo "</p>
                                ";
            }
            // line 96
            echo "                                ";
            if ($this->getAttribute($context["itemScreen"], "blocks_info", array())) {
                // line 97
                echo "                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["itemScreen"], "blocks_info", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["itemBlockInfo"]) {
                    // line 98
                    echo "                                        <div class=\"aste-block-number\">
                                            <div class=\"aste-number-label\">";
                    // line 99
                    echo $this->getAttribute($context["itemBlockInfo"], "title_block_info", array());
                    echo "</div>
                                            <div class=\"aste-number\">";
                    // line 100
                    echo $this->getAttribute($context["itemBlockInfo"], "content", array());
                    echo "</div>
                                        </div>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemBlockInfo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 103
                echo "                                ";
            }
            // line 104
            echo "                                ";
            if (($this->getAttribute($context["itemScreen"], "url_link", array()) && $this->getAttribute($context["itemScreen"], "text_link", array()))) {
                // line 105
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemScreen"], "url_link", array()), "html", null, true);
                echo "\" class=\"aste-block-link\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["itemScreen"], "text_link", array()), "html", null, true);
                echo " <img src=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/text-arrow.svg");
                echo "\" /></a>
                                ";
            }
            // line 107
            echo "                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemScreen'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 113
        echo "
    </div>
</div>



<div class=\"astb-accom-slider\">
    <div class=\"container\">
        <h2 class=\"aste-primary\">";
        // line 121
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_bold_locations"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h2>
        <h3 class=\"aste-secondary\">";
        // line 122
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("title_definition_locations"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h3>
        <div class=\"aste-accom-slider\">
            ";
        // line 124
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["locations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["itemLocation"]) {
            // line 125
            echo "                <div class=\"aste-slide\">
                    <div class=\"aste-slide-image-wrapper\"><img src=\"";
            // line 126
            echo call_user_func_array($this->env->getFilter('resize')->getCallable(), array($this->getAttribute($context["itemLocation"], "image_preview", array()), 350, 230));
            echo "\" alt=\"";
            echo $this->getAttribute($this->getAttribute($context["itemLocation"], "image_preview", array()), "description", array());
            echo "\" title=\"";
            echo $this->getAttribute($this->getAttribute($context["itemLocation"], "image_preview", array()), "title", array());
            echo "\" class=\"aste-slide-image\"></div>
                    <div class=\"aste-accom-title\">";
            // line 127
            echo $this->getAttribute($context["itemLocation"], "title", array());
            echo "</div>
                    <div class=\"aste-accom-address\">";
            // line 128
            echo $this->getAttribute($context["itemLocation"], "address", array());
            echo "</div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['itemLocation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 131
        echo "        </div>
    </div>
</div>



<div class=\"astb-bottom-links\">
    <a href=\"/locations/\" class=\"aste-bottom-link astm-places\">Все места</a>
    <a href=\"/contacts/\" class=\"aste-bottom-link astm-contact\">Свяжитесь с нами <img src=\"";
        // line 139
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/text-arrow.svg");
        echo "\" /></a>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  358 => 139,  348 => 131,  339 => 128,  335 => 127,  327 => 126,  324 => 125,  320 => 124,  313 => 122,  307 => 121,  297 => 113,  286 => 107,  276 => 105,  273 => 104,  270 => 103,  261 => 100,  257 => 99,  254 => 98,  249 => 97,  246 => 96,  240 => 94,  237 => 93,  231 => 91,  229 => 90,  219 => 85,  212 => 80,  208 => 79,  197 => 73,  191 => 72,  185 => 71,  174 => 62,  161 => 59,  153 => 58,  150 => 57,  146 => 56,  133 => 48,  127 => 47,  123 => 45,  114 => 42,  110 => 41,  107 => 40,  103 => 39,  94 => 35,  88 => 34,  82 => 33,  73 => 26,  63 => 22,  59 => 21,  53 => 18,  50 => 17,  46 => 16,  36 => 8,  34 => 7,  31 => 6,  29 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set recordsProcents = ProcentBlocks.records %}

{% set recordsPromo = PromoSlider.records %}

{% set recordsScreens = Screens.records %}

{% set recordsSlider = Slider.records %}


<div class=\"astb-header-slider\">
    <div class=\"aste-arrows\">
        <div class=\"aste-arrow astm-left\"></div>
        <div class=\"aste-container-sized-delimiter container\"></div>
        <div class=\"aste-arrow astm-right\"></div>
    </div>
    {% for itemSlider in recordsSlider %}
        <div class=\"aste-slider-block\">
            <img class=\"aste-block-bg\" src=\"{{ itemSlider.slide_image.path }}\" />
            <div class=\"aste-block-shade\"></div>
            <div class=\"aste-block-text\">
                <h2 class=\"aste-primary\">{{ itemSlider.title_bold }}</h2>
                <h3 class=\"aste-secondary\">{{ itemSlider.title_definition }}</h3>
            </div>
        </div>
    {% endfor %}
</div>



<div class=\"astb-main-description\">
    <div class=\"container aste-description-container\">
        <div class=\"aste-description-left astm-border-right\">
            <h1 class=\"aste-primary\">{% component 'title_bold_company' %}</h1>
            <h2 class=\"aste-secondary\">{% component 'title_definition_company' %}</h2>
            <p class=\"aste-description\">{% component 'text_company' %}</p>
        </div>
        <div class=\"aste-description-right\">
            <div class=\"aste-percentages\">
                {% for itemProcent in recordsProcents %}
                    <div class=\"aste-percentage\">
                        <div class=\"aste-number\">{{ itemProcent.procent }}</div>
                        <div class=\"aste-percentage-title\">{{ itemProcent.definition }}</div>
                    </div>
                {% endfor %}
            </div>
        </div>
        <div class=\"aste-description-left astm-description-link\">{% component 'link_about' %}</div>
        <div class=\"aste-description-right astm-description-link\">{% component 'link_audience' %}</div>
    </div>
</div>



<div class=\"astb-screens-slider\">
    <div class=\"aste-slider-track\">
        {% for itemPromo in recordsPromo %}
            <div class=\"aste-screen-wrapper\">
                <div class=\"aste-frame\"><img src=\"{{ itemPromo.promo_pattern.path | resize(290,20) }}\" alt=\"{{ itemPromo.promo_pattern.description }}\" title=\"{{ itemPromo.promo_pattern.title }}\"class=\"aste-frame-texture\"></div>
                <div class=\"aste-screen\"><img src=\"{{ itemPromo.promo_slide_image.path | resize(290,520) }}\" alt=\"{{ itemPromo.promo_slide_image.description }}\" title=\"{{ itemPromo.promo_slide_image.title }}\" class=\"aste-screen-image\"></div>
            </div>
        {% endfor %}
    </div>
</div>


<div class=\"astb-screens-info\">
    <div class=\"aste-info-introductory\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 mx-auto\">
                    <h2 class=\"aste-primary\">{% component 'title_bold_screens' %}</h2>
                    <h3 class=\"aste-secondary\">{% component 'title_definition_screens' %}</h3>
                    <p class=\"aste-introductory-text\">{% component 'text_screens' %}</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"aste-info-blocks\">
        {% for itemScreen in recordsScreens %}
            <div class=\"aste-info-block\">
                <div class=\"container\">
                    <div class=\"row aste-block-row\">
                        <div class=\"d-none d-lg-block col-2\"></div>
                        <div class=\"col-4 col-lg-3\">
                            <div class=\"aste-block-image-wrapper\"><img src=\"{{ itemScreen.image_screen.path|raw|resize(270,478) }}\" alt=\"{{ itemScreen.image_screen.description }}\" class=\"aste-block-image\"></div>
                        </div>
                        <div class=\"d-none d-sm-block col-sm-1\"></div>
                        <div class=\"col-8 col-sm-7 col-lg-4\">
                            <div class=\"aste-block-content\">
                                {% if itemScreen.title %}
                                    <h4 class=\"aste-block-title\">{{ itemScreen.title }}</h4>
                                {% endif %}
                                {% if itemScreen.text %}
                                    <p class=\"aste-block-text\">{{ itemScreen.text }}</p>
                                {% endif %}
                                {% if itemScreen.blocks_info %}
                                    {% for itemBlockInfo in itemScreen.blocks_info %}
                                        <div class=\"aste-block-number\">
                                            <div class=\"aste-number-label\">{{ itemBlockInfo.title_block_info|raw }}</div>
                                            <div class=\"aste-number\">{{ itemBlockInfo.content|raw }}</div>
                                        </div>
                                    {% endfor %}
                                {% endif %}
                                {% if itemScreen.url_link and itemScreen.text_link %}
                                    <a href=\"{{ itemScreen.url_link }}\" class=\"aste-block-link\">{{ itemScreen.text_link }} <img src=\"{{ 'assets/img/text-arrow.svg'|theme }}\" /></a>
                                {% endif %}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}

    </div>
</div>



<div class=\"astb-accom-slider\">
    <div class=\"container\">
        <h2 class=\"aste-primary\">{% component 'title_bold_locations' %}</h2>
        <h3 class=\"aste-secondary\">{% component 'title_definition_locations' %}</h3>
        <div class=\"aste-accom-slider\">
            {% for itemLocation in locations %}
                <div class=\"aste-slide\">
                    <div class=\"aste-slide-image-wrapper\"><img src=\"{{ itemLocation.image_preview|resize(350,230) }}\" alt=\"{{ itemLocation.image_preview.description|raw }}\" title=\"{{ itemLocation.image_preview.title|raw }}\" class=\"aste-slide-image\"></div>
                    <div class=\"aste-accom-title\">{{ itemLocation.title|raw }}</div>
                    <div class=\"aste-accom-address\">{{ itemLocation.address|raw }}</div>
                </div>
            {% endfor %}
        </div>
    </div>
</div>



<div class=\"astb-bottom-links\">
    <a href=\"/locations/\" class=\"aste-bottom-link astm-places\">Все места</a>
    <a href=\"/contacts/\" class=\"aste-bottom-link astm-contact\">Свяжитесь с нами <img src=\"{{ 'assets/img/text-arrow.svg'|theme }}\" /></a>
</div>", "/var/www/www-root/data/www/october.asteam/themes/as-team/pages/home.htm", "");
    }
}
